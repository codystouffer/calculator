package org.eclipse.example.calc.internal.operations;

import org.eclipse.example.calc.BinaryOperation;

public class Power extends AbstractOperation implements BinaryOperation {

	@Override
	public float perform(float arg1, float arg2) {
		float x;
		for (x = arg2; x > 0; x--) {
			arg1 *= arg2;
		}
		return arg1;
	}

	@Override
	public String getName() {
		return "^";
	}
}